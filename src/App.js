import React, { Component } from "react";
import { Provider } from "mobx-react";
import './App.css';
import Overview from './components/overview';
import Store from './stores/store';
import Field from "./components/field";


class App extends Component {
  store = new Store();
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false    };
  }
   // Fetch the JSON data.
   _fetchJSON = async () => {
    const response = await fetch(this.props.endpoint, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    const body = await response.json();
    return body;
  };
  componentDidMount = () => {
    // Fetch the data after the component mounted
    this._fetchJSON()
      .then(data => {
        if (data) {
          console.log(data);
          // Set the loaded state so the app knows how to continue
          this.store.data = data;
          this.store.filteredPlayers = data.players;
          this.setState({
            isLoaded: true
          });
        }
      })
      .catch(err => {
        this.setState({
          error: "there has been an error fetching the data."
        });
      });
  }
  render = () => {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>{this.state.error}</div>;
    } else if (!isLoaded) {
      return <div>loading...</div>;
    } else {
      return ( <Provider store={this.store}>
    
        <Field />
        <Overview />

      </Provider>
    );
  }
  
}
}

export default App;
