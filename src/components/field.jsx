import React, { Component } from "react";
import { inject } from "mobx-react";
import Position from './position';
import '../style/field.scss';


@inject("store")
class Field extends Component {
    state = { 
        active: false
    }
    handleClick = (e) => {
this.props.store.handleFilter(this.props.data.tid);
this.setState({active: !this.state.active});
  }
    render () {
    return (
    <div className='field--wrapper'>
        <div className='vertical--line left--line'></div>
        <Position filter={this.props.store.data.filters[0]} amount={2}></Position>
        <Position filter={this.props.store.data.filters[1]} amount={2}></Position>
        <div className='vertical--line right--line'></div>
        <Position filter={this.props.store.data.filters[2]} amount={3}></Position>
    </div>)
    }
}
export default Field;