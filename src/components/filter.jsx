import React, { Component } from "react";
import { observer, inject } from "mobx-react";

@inject("store")
class Filter extends Component {
    state = { 
        active: false
    }
    handleClick = (e) => {
this.props.store.handleFilter(this.props.data.tid);
this.setState({active: !this.state.active});
  }
    render () {
    return (<div className={`filter--container ${this.state.active ? 'filter-active' : 'filter-non-active'}`} onClick={this.handleClick}><img style={{width:'140px'}} src={this.props.data.image}/>{this.state.active ? '1' : '0'}</div>)
    }
}
export default Filter;