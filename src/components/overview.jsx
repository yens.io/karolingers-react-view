import React, { Component } from "react";
import Player from './player';
import { observer, inject } from "mobx-react";
import '../style/overview.scss';


@inject("store")
@observer
class Overview extends Component {
    render () {
        return (
            <div className='players--overview'>
        {this.props.store.filteredPlayers.map(player => {
            return <Player player={player} key={player.nid}/>
        })}
        </div>)
    }
}
export default Overview;