import React from "react";
import "../style/player.scss";

const Player = props => {
  return (
    <div className="player--container">
        <div className='player--container--wrapper'>
      <div className="player--hover--container">
        <div className="player--number">
        <div className='player--number--wrapper'>{props.player.number}</div>
        </div>
        <img className="player--image" src={props.player.image}></img>
      </div>
      <div className="player--name">
          <a  className='player--name--link' href={'/node/' + props.player.nid}>{props.player.name}</a></div>
      </div>
    </div>
  );
};
export default Player;
