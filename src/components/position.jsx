import React, { Component } from "react";
import { inject } from "mobx-react";
import _ from 'lodash'

@inject("store")
class Position extends Component {
    state = { 
        active: false
    }
    handleClick = (e) => {
this.props.store.handleFilter(this.props.filter.tid);
this.setState({active: !this.state.active});
  }
  generateGroup = (amount) => {
      let group = [];
      _.times(amount, (i) => {
          group.push(<div className={`position--container ${this.state.active ? 'position-active' : 'position-non-active'} position--index--${i} `} onClick={this.handleClick}><div></div></div>)
      });
      return group;
  }
    render () {
    return (<div className={`positions--group positions--group--label--${this.props.filter.label} positions--group--tid--${this.props.filter.tid}`}>{this.generateGroup(this.props.amount)}</div>)
    }
}
export default Position;