import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

let endpoint = document
  .getElementById("players_overview")
  .getAttribute("data-endpoint");
ReactDOM.render(<App endpoint={ endpoint }/>, document.getElementById('players_overview'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
