import { observable } from "mobx";
class Store { 
    @observable selectedFilters = [];
    @observable data = [];
    @observable filteredPlayers = [];

    clearFilters = () => {
        this.selectedFilters = [];
    }
    clearFilter = (tid) => {
        this.selectedFilters = this.selectedFilters.filter(filter => {
            return filter !== tid;
        })
    }
    handleFilter = (tid) => {
        if (!this.selectedFilters.includes(tid)) {
            this.selectedFilters = [...this.selectedFilters, tid]
        }
        else {
            this.clearFilter(tid);
        }
        this.filterPlayers(tid);
    }
    checkFilter = (tid) => {
        if (this.selectedFilters.length > 0) {
            return this.selectedFilters.includes(tid);
        }
        else {
            return true;
        }
    } 
    filterPlayers = (tid) => {
        this.filteredPlayers = this.data.players.filter(player => {
            return this.checkFilter(player.position);
        });
    }
}
export default Store;